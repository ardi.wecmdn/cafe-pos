<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryCtrl extends Controller
{
    function index(){
        // Response list data 

        $data = [
            "title" => "Category",
            "dtCat" => Category::All()
        ];

        return view("category.data",$data);
    }

    function form(Request $req){
        // Form add or edit
        $data = [
            "title" => "Category",
            "rsCat" => Category::where("id",$req->id_cat)->first()
        ];

        return view("category.form",$data);        
    }

    function save(Request $req){
        // Create or Update
        // Validation
        $req->validate(
            [
                "cat_nm" => "required|unique:categories|max:30"
            ],
            [
                "cat_nm.required"=>"Wajib diisi !",
                "cat_nm.unique"=>"Maaf category sudah ada",
                "cat_nm.max"=>"Maximal 30 Karakter",
            ]
        );

        try {
            // Save
            Category::updateOrCreate(
                [
                    "id" => $req->input("id_cat")
                ],
                [
                    "cat_nm" => $req->input("cat_nm")
                ]
            );

            // Notif 
            $notif = [
                "type" => "success",
                "text" => "Data Berhasil Disimpan !"
            ];

        } catch(Exception $err){
            $notif = [
                "type" => "success",
                "text" => "Data Gagal Disimpan !".$err->getMessage()
            ];
        }

        return redirect(url("category"))->with($notif);
    }

    function delete($id){
        // Delete Data
        try {
            // Save
            Category::where("id",$id)->delete();

            // Notif 
            $notif = [
                "type" => "success",
                "text" => "Data Berhasil Dihapus !"
            ];

        } catch(Exception $err){
            $notif = [
                "type" => "success",
                "text" => "Data Gagal Dihapus !".$err->getMessage()
            ];
        }

        return redirect(url("category"))->with($notif);        
    }
}
