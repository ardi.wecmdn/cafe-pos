@extends('layouts.template')

@section("title","Data Customers")
@section("page-title","Data Customers")

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <a class="btn btn-block btn-primary btn-sm" href="">Add New</a>
            </div>
        </div>
        <div class="card-body">
            <table id="dtCustomer" class="dtTable table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 4.0
                    </td>
                    <td>Win 95+</td>
                    <td> 4</td>
                    <td>X</td>
                  </tr>
                </tbody>
            </table>            
        </div>
    </div>
@endsection